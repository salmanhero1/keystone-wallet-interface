import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import { Parallax } from 'react-parallax';

import limitedMiningIcon from '../assets/images/facts/mining_round.png';
import maxSupplyIcon from '../assets/images/facts/maxsupply_round.png';
import newBlockchainIcon from '../assets/images/facts/blockchain_round.png';
import roadmapIcon from '../assets/images/facts/smartcontracts_round.png';
import communityIcon from '../assets/images/facts/community_round.png';
import webbasedAccIcon from '../assets/images/facts/webbased_round.png';
import decentralizedIcon from '../assets/images/facts/decentralized_round.png';
import bcryptIcon from '../assets/images/facts/algo_round.png';
import proofOfStakeIcon from '../assets/images/facts/pos_round.png';
import parallaxBackground from '../assets/images/header_bg.jpg';
import walletIcon from '../assets/images/wallet.png';
import Image_01 from  '../assets/images/IMG-20180118-WA0048.jpg';
import Image_02 from '../assets/images/IMG-20180118-WA0057.jpg';
import Image_03 from '../assets/images/IMG-20180118-WA0050.jpg';
//import publicationOne from '../assets/downloads/pdf/LaTortuga/publication_01__02_15_2018.pdf';

class DefaultPage extends Component {
    constructor(){
      super();
      this.thumbnailSlider.bind(this);      
    }
    
    componentWillMount(){
      this.thumbnailSlider();
    }
    
    thumbnailSlider() {      				
          const slides = [
                   
                <div className="row pt-3" key={1}>
                   <div className="col-12 col-sm-6 col-md-4 col-lg-4">
                      <div className="container text-center">
                        <h3><strong>Limited mining.</strong></h3>
                        <div className="col-sm-6 col-md-8 mx-auto"><img className="img-fluid" src={limitedMiningIcon} alt=""/></div>
                        <p>Switches to PoS at 35m circulation</p>
            
                      </div>
                    </div>            
                    <div className="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div className="container text-center">
                        <h3><strong>Proof of Stake</strong></h3>
                        <div className="col-sm-6 col-md-8 mx-auto"><img className="img-fluid" src={proofOfStakeIcon} alt=""/></div>
                        <p>After mining ends.</p>
                      </div>
                    </div>            
                    <div className="col-12 col-sm-6 col-md-4 col-lg-4">
                          <div className="container text-center">
                        <h3><strong>Limited Supply.</strong></h3>
                        <div className="col-sm-6 col-md-8 mx-auto"><img className="img-fluid" src={maxSupplyIcon} alt=""/></div>
                        <p>150,000,000 KEYS.</p>
                      </div>
                    </div>
                  </div>,
 			<div className="row pt-3" key={2}>                
				<div className="col-12 col-sm-6 col-md-4 col-lg-4">
 					<div className="container text-center">
						<h3><strong>Community Adoption.</strong></h3>
 						<div className="col-sm-6 col-md-8 mx-auto"><img className="img-fluid" src={communityIcon} alt=""/></div>
 						<p> A real community <br/>fully adopting Keystone.</p>
 					</div>
 				</div>

 				<div className="col-12 col-sm-6 col-md-4 col-lg-4">
 					<div className="container text-center">
						<h3><strong>Smart Contracts</strong></h3>
 						<div className="col-sm-6 col-md-8 mx-auto"><img className="img-fluid" src={roadmapIcon} alt=""/></div>
 						<p>Buy and Sell real assets. <br/>On Roadmap</p>
 					</div>
 				</div>

 				<div className="col-12 col-sm-6 col-md-4 col-lg-4">
 					<div className="container text-center">
						<h3><strong>Web-based Wallet</strong></h3>
 						<div className="col-sm-6 col-md-8 mx-auto"><img className="img-fluid" src={webbasedAccIcon} alt=""/></div>
 						<p>Make transactions from any web browser.</p>
 					</div>
 				</div>
      </div>,        
 			<div className="row pt-3" key={3}>
        <div className="col-12 col-sm-6 col-md-4 col-lg-4">
					<div className="container text-center">
						<h3><strong>Bcrypt Hashing</strong></h3>
 						<div className="col-sm-6 col-md-8 mx-auto"><img className="img-fluid" src={bcryptIcon} alt=""/></div>
 						<p>Fair mining for all.</p>
 					</div>
 				</div>
 				<div className="col-12 col-sm-6 col-md-4 col-lg-4">
					<div className="container text-center">
						<h3><strong>Decentralized</strong></h3>
 						<div className="col-sm-6 col-md-8 mx-auto"><img className="img-fluid" src={decentralizedIcon} alt=""/></div>
 						<p>Take back control.</p>
 					</div>
 				</div>
				<div className="col-12 col-sm-6 col-md-4 col-lg-4">
 					<div className="container text-center">
						<h3><strong>New Blockchain Technology</strong></h3>
 						<div className="col-sm-6 col-md-8 mx-auto"><img className="img-fluid" src={newBlockchainIcon} alt=""/></div>
 						<p>Completely written in JavaScript. </p>
 					</div>
 				</div>
      </div>];
      this.setState({slides:slides[Math.floor(Math.random()*slides.length)]});
      setTimeout(this.thumbnailSlider.bind(this), 3000);        
    }
  
    render() {
      return (
  <div >
          <Header />

 <section>
 	<div className="container-fluid px-0">
 
                <Parallax bgImage={parallaxBackground} strength={400}>
<div className="container my-5">
<div style={{height:100 + 'px'}} />
       <div className="color4 pt-5 pb-2 my-5 rounded jumbotron" style={{backgroundColor: '#9ab7af33'}}>
             <div className="row">
                   <div className="col text-center" style={{textShadow: '2px 2px 4px black'}}>
                          <h1 className="text-white display-2 d-none d-lg-block d-xl-block" ><span>Keystone</span> CURRENCY</h1>
                          <h1 className="text-white display-4 d-block d-lg-none d-xl-none" ><span>Keystone</span> CURRENCY</h1>
                   </div>
              </div>

              <div className="row">
                    <div className="col text-center">
                             <h2 className="text-white pb-5" style={{textShadow: '2px 2px 4px black'}}>Introducing The World&#39;s First Blockchain City</h2>

                    </div>
             </div>
       </div>
<div style={{height:130 + 'px'}} />
</div>
		</Parallax>

 	</div>
 </section>


 <section>
 	<div className="container-fluid" id="aboutus">
 		<div className="container pb-0">
 			<div className="row">
 				<div className="col text-center">
 					<h1 className="color14 pt-1 x-large">Welcome To <span style={{textShadow: '2px 2px 4px #acccc2'}}>Keystone Currency</span></h1>
     </div>
 			</div>
 		</div>
 	</div>
 </section>


    <section>
    <div className="container-fluid color6-bg">
   			<div className="container py-5">
   				<div className="row">
        <div className="col align-content-center text-justify">
         <h2 className="color13 pb-3" ><strong>Redefining Progress</strong></h2>
         <p className="color14">World-changing technologies, like the blockchain, have always brought times of unrestricted wealth, but also negative impacts on the environment, which makes us wonder if we’re truly advancing at all.</p>
         <p className="color14">Keystone provides the answer.</p>
   					</div>
       </div>
      </div>
     </div>
   </section>



  <section>
 	<div className="container-fluid"  id="aboutus">
 		<div className="container">
 			<div className="row">
 				<div className="col text-center">
 					<h1 className="green"><span >Keystone Currency Numbers</span></h1>
 				</div>
 			</div>
 			<div className="mb-5"></div>
 			<div className="row">
				<div className="col-12 col-sm-6 col-md-4 col-lg-4">
					<div className="progress green">
						<span className="progress-right">
							<span className="progress-bar"></span>
						</span>
						<div className="progress-value">35M Available <br/> for POW</div>
					</div>
				</div>
				<div className="col-12 col-sm-6 col-md-4 col-lg-4">
					<div className="progress blue">
						<span className="progress-left">
							<span className="progress-bar"></span>
						</span>
						<span className="progress-right">
							<span className="progress-bar"></span>
						</span>
						<div className="progress-value">115m Available for POS</div>
					</div>
				</div>

				<div className="col-12 col-sm-6 col-md-4 col-lg-4">
					<div className="progress yellow">
						<span className="progress-left">
							<span className="progress-bar"></span>
						</span>
						<span className="progress-right">
							<span className="progress-bar"></span>
						</span>
						<div className="progress-value">150M Max Supply</div>
					</div>
				</div>
 			</div>
 		</div>
 	</div>
 </section>

   <section>
    <div className="container-fluid color6-bg" id="numbers">
   			<div className="container py-5">
   				<div className="row">
        <div className="col align-content-center text-justify">
         <h2 className="color13 pb-3"  ><strong>Keystone (KEYS)</strong></h2>
         <p className="color14">Keystone seamlessly merges the blockchain with sustainable development. It offers the model for a new standard of living, where wealth and luxury happily co-exist with eco-friendliness.</p>
   					</div>
       </div>
      </div>
     </div>
   </section>

     <section>
   		<div className="container-fluid" id="wallet">
   			<div className="container">
   				<div className="row">
   					<div className="col-sm-9 col-12 col-md-9 col-lg-9 align-content-center text-center">
   						<h2 className="pb-3 pt-3 text-left">Get your Keystone Currency Wallet Now</h2>
         <Link className="btn btn-lg green_bg align-content-center" to="/Wallet/Create">Create Your Wallet</Link>
   					</div>
   					<div className="col-sm-3 col-12 col-md-3 col-lg-3 align-self-center"><img src={walletIcon} alt=""/></div>
   				</div>
   			</div>
   		</div>
   </section>


<section>
 	<div className="container-fluid green_bg color14" >
 		<div className="container py-5">
 			<div className="row pb-2">
 				<div className="col text-center">
 					<h1 className="color4"><span >Keystone</span> Currency Facts</h1>
 				</div>
 			</div>
      {this.state ? this.state.slides : ''}			
 		</div>
 	</div>
 </section>

   <section>
    <div className="container-fluid color6-bg" id="numbers">
   			<div className="container py-5">
   				<div className="row">
        <div className="col align-content-center text-justify">
         <h2 className="color13 pb-3" ><strong>Features</strong></h2>
         <p className="color14">Hyper-Secure Web Wallet - You are in control, not us. <br/>
Self-Hosted Exchange - Buy KEYS directly with USD or Bitcoin. <br/>
Limited, POW mining, Switch to energy efficient POS at 35m Circulation. <br/>
A project you can see, touch and feel. </p>
   					</div>
       </div>
      </div>
     </div>
   </section>

 <section>
 	<div className="container-fluid" id="aboutus">
 		<div className="container pb-0">
 			<div className="row">
 				<div className="col text-center color14">
     					<h2>Keystone - <span  >La Tortuga</span></h2>
 					<h3>About Keystone Currency</h3>
				</div>
			</div>
<div className="row">
	<div className="col-md-4"><img className="rounded img-fluid" src={Image_01} alt=""/></div>
	<div className="col-md-8 align-self-center" >
	<div className="card color4-bg">
   <div className="card-body">
      		<p className="text-justify">The mission of Keystone is to seamlessly merge blockchain with sustainable development. We believe in a future of blockchain beyond the cryptocurrency ecosystem. We believe just as much, if not more, in raising the standard of living in the 21st century. Keystone is the product of these two beliefs working in unison.</p> 
        <p>And La Tortuga is the crown jewel of our efforts.</p>
        
   </div>

	</div>
	</div>
</div>
<div className="row py-2">
        <div className="col-md-8 align-self-center" >
	<div className="card">
      		<div className="card-body text-justify color14">
          <p>La Tortuga is a sustainable community project based in northwestern Mexico. It comes equipped with the latest advancements in eco-friendly technology, and a community supported by highly trained educators, top researchers, and government officials.</p>
          <p>It also offers you the opportunity of a lifetime. You can do your part in helping the planet, while enjoying the fruits of a good investment.</p>
        </div>
	</div>
	</div>
	<div className="col-md-4 align-self-center"><img className="rounded img-fluid" src={Image_02} alt=""/></div>
</div>
<div className="row">
	<div className="col-md-4"><img className="rounded img-fluid" src={Image_03} alt=""/></div>
        <div className="col-md-8 align-self-center" >
	<div className="card color4-bg">
      		<div className="card-body text-justify color17">
         <p>More information about La Tortuga will be available as we continue development.  We hope you enjoyed this brief preview and will join us on the journey to freedom, wealth, and the wellbeing of our planet.</p>
        </div>
	</div>
	</div>
</div>

     
 			
 		</div>
 	</div>
 </section>

   <section>
    <div className="container-fluid color6-bg" id="numbers">
   			<div className="container py-5">
   				<div className="row">
        <div className="col align-content-center text-justify">
         <h2 className="color13 pb-3" ><strong>The Ultimate Use-Case</strong></h2>
         <p className="color14">Keystone is not another cryptocurrency backed by abstract promises. It is the backbone of a large-scale development project that redefines what it means to live in the 21st Century.</p>
         <p className="color14">Subscribe to the Keystone Newsletter for more info.</p>
   					</div>
       </div>
      </div>
     </div>
   </section>


   <Footer />
    </div>
    );
    }
  }
  export default withRouter(DefaultPage);