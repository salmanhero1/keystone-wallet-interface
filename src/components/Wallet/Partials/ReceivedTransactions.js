import React from 'react';
import { connect } from 'react-redux';
import { Card, CardHeader } from 'reactstrap';
import TransactionPagination from './TransactionPagination';
export default connect()((props) => {
        return (<Card className="mx-0 my-0">
                    <CardHeader className="color7-bg" id="ReceivedTransactionHeader">
                        <h3 className="mb-0 color4" data-toggle="collapse" data-target="#collapseReceivedTransaction" aria-expanded="true" aria-controls="collapseReceivedTransaction">
                            <strong className="display3"> Received </strong>
                            <span className="float-right"> Total: {props.transactions.length ? props.transactions.length : <i className="fa fa-spnner fa-spin"></i>} </span>
                        </h3>
                    </CardHeader>
                    <div id="collapseReceivedTransaction" className="collapse hide" aria-labelledby="ReceivedTransactionHeader">
                        { props.transactions.length ? 
                        <TransactionPagination transactions={props.transactions} page={0} limit={15} received={true}/>
                        : ''}
                    </div>
                </Card>);
})
