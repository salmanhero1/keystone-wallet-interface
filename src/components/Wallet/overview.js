import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter} from 'react-router-dom';
import Header from './../Header';
import Footer from './../Footer';
import fileDownload from 'js-file-download';
import axios from 'axios';
import HOST_URL from '../../configs';
import SentTransactions from './Partials/SentTransactions';
import ReceivedTransactions from './Partials/ReceivedTransactions';
import AwardedBlocks from './Partials/AwardedBlocks';
const stylePointer = { cursor:"pointer"};
class WalletOverview extends Component {
    state = {
      balance:0,
      blocks:[],
      totalBlocks:0,
      includesGenesis:false,
      transactions:[],
      sentTransactions:[],
      receivedTransactions:[]
    };
 
    componentWillMount(){
      axios.get(`${HOST_URL}/Wallet/Balance`, {headers:{Authorization:JSON.stringify(this.props.auth.wallet.walletPublicKey)}})
        .then((balance) => {
          axios.get(`${HOST_URL}/Wallet/Transaction`, {headers:{Authorization:JSON.stringify(this.props.auth.wallet.walletPublicKey)}})
                .then((transactions) => {
                    axios.get(`${HOST_URL}/Wallet/Awards`, {headers:{Authorization:JSON.stringify(this.props.auth.wallet.walletPublicKey)}})
                      .then((response) => {
                        this.setState({
                         blocks:response.data.blocks,
                         totalBlocks:response.data.totalBlocks,
                         includesGenesis:response.data.includesGenesis,
                         balance:balance.data.balance,        
                         transactions:transactions.data.transactions,
                         sentTransactions: transactions.data.transactions.filter(transaction => transaction.sender.a === this.props.auth.wallet.walletPublicKey.a && transaction.sender.b === this.props.auth.wallet.walletPublicKey.b),
                         receivedTransactions: transactions.data.transactions.filter(transaction => transaction.receiver.a === this.props.auth.wallet.walletPublicKey.a && transaction.receiver.b === this.props.auth.wallet.walletPublicKey.b)
                        });
                      })
                      .catch(err => {console.log(err);});
                })
                .catch(err => {console.log(err);});
        })
        .catch(err => {
          console.log(err);
        });    
    }
    
    downloadImage(){
        // Grab the extension to resolve any image error
        const ext = this.qrImgBase64.split(';')[0].match(/jpeg|png|gif/)[0];
        // strip off the data: url prefix to get just the base64-encoded bytes
        const data = this.qrImgBase64.replace(/^data:image\/\w+;base64,/, "");
        const buf = new Buffer(data, 'base64');
        fileDownload(buf, 'image.' + ext);
    }
    
    render() {
      let imageQR = this.props.auth.wallet.getQRImage();
      let imgHtml = imageQR.split('/>');
      let base = imgHtml[0];
      this.qrImgBase64 = null;
      base = base.split(' ').reduce((s, v) => {
        if(v !== undefined){
          if(this.qrImgBase64 === null){
            let params = v.split('=');
            if(params[0] === 'src') {
                this.qrImgBase64 = params[1].substr(1,params[1].length - 2); 
                return this.qrImgBase64;            
            }
          }else{
            return s;
          }
        }
        return s;
      });
      this.qrImgBase64 = base;
      let imgFrontLink = [imgHtml.shift()];
      imgFrontLink.push(' class="img-qr-code"');
      imgFrontLink.push('/>');
      imgFrontLink.push(`<div class="qr-text"> Click to Download </div>`);
      imageQR = imgFrontLink.join('');
      return (
          <div>
            <section>
                <Header />
            </section>
            <section>
                <div className="container-fluid" id="aboutus">
                    <div className="container">
                        <div className="row ">
                            <div className="col text-center">
                                <h1 className="green">Wallet Explorer</h1>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header color15-bg color5 text-center px-2 py-4">
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-3 col-lg-3 align-self-center">
                                        <h2 className="pt-0 pb-2 text-center"><strong>Balance</strong></h2>
                                        <h2>{"\u26B7"}<small>{this.state.balance ? this.state.balance: 0}</small></h2>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 my-0 py-0 px-1 align-self-center align-content-center">
                                        <h3>Public Key</h3>
                                        <pre className="color4 pt-2" style={stylePointer} title="Click to Copy to Clipboard" onClick={(e)=>{
                                          var textField = document.createElement('textarea');
                                          textField.innerText = e.target.innerText;
                                          document.body.appendChild(textField);
                                          textField.select();
                                          document.execCommand('copy');
                                          textField.remove();                                          
                                          }}>{this.props ? this.props.auth.wallet.walletPublicKey.a + '\n' + this.props.auth.wallet.walletPublicKey.b: ''}</pre>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-3 col-lg-3 align-self-center pr-0">
                                        <a onClick={this.downloadImage.bind(this)}>
                                            <div className="thumbnail" dangerouslySetInnerHTML={{ __html: imageQR }} />
                                        </a>
                                    </div>
                              </div>
                            </div>
                            <div className="card-body mx-0 my-0 px-0 py-0">
                                <div className="row justify-content-center no-gutters">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                      { this.state.sentTransactions ?
                                          <SentTransactions transactions={this.state.sentTransactions} />
                                          : ''}
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                      { this.state.receivedTransactions ?
                                      <ReceivedTransactions transactions={this.state.receivedTransactions} />
                                        : ''}
                                    </div>
                                    <div className="col">
                                      <AwardedBlocks blocks={this.state.blocks || []}  totalBlocks={this.state.totalBlocks || 'N/A'}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>  
                <Footer />
            </section>
          </div>);
    }
  }
  
  const mapStateToProps = state => {
  // add spent tansactions and unspent transactions
  return {
    auth: state.auth,
    error: state.auth.error
  };
};

export default withRouter(connect(mapStateToProps)(WalletOverview));
