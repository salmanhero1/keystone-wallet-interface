import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { reduxForm, Field, change as changeFieldValue } from 'redux-form';
import { getLastTransaction, signTransaction, hashTransaction, sendTransaction } from '../../actions';
import Header from '../Header';
import Footer from '../Footer';
import crypto from 'crypto';
import QrReader from 'react-qr-reader';
class SendTransaction extends Component {
  constructor(props){
    super(props);
    this.state = {
        error: props.error,
        progress:props.progress,
        users: props.users,
        transaction:props.transaction,
        auth:props.auth,
        hash: props.transaction.txHash,
        encrypted: false,
        delay: 300,
        result: 'No result',
        showCamera: false,
      };
    this.handleGeneratePassphrase.bind(this);
    this.renderQRReader = this.renderQRReader.bind(this);
    this.handleScan = this.handleScan.bind(this);
    this.handleSubmit = props.handleSubmit.bind(this);
  }
  handleGeneratePassphrase() {
      this.refs.saltSpinner.classList.add('fa-spin');
      this.props.changeFieldValue(this.props.form, 'passphrase', crypto.randomBytes(16).toString('hex'));
      setTimeout(()=>{this.refs.saltSpinner.classList.remove('fa-spin');}, 1000);      
  }
  
  checkMessageLength(message) {
    if(message !== undefined){
      if(message.length < 255) return false;
      return true;
    }
    return true;
  }
  
  handleFormSubmit(formElements){
    let { receiver, amount, stakeAmount, message } = formElements;
    const { auth, history } = this.props;
    if( auth === undefined || auth.wallet === undefined) {
      this.setState({
        progress: 'error',
        error:'Error :: Authentication is required to proceed.'
      });
    }
    if(receiver.length < 128) {
      this.setState({
        progress: 'error',
        error:'Max message length is a total off 255 characters.'
      });
    }
    if(this.checkMessageLength(message)){
      this.setState({
        progress: 'Error',
        error:'Error :: Message length is a max total of 255 characters.'
      });
    }
    if(this.state.encrypted) {
        let { cipherAlgo , passphrase } = formElements;
        if(cipherAlgo === undefined) cipherAlgo = 'des-ecb';
        console.log('wallet enc', cipherAlgo, passphrase);
        const cipher = crypto.createCipher(cipherAlgo, passphrase);
        let crypted = cipher.update(message, 'utf-8', 'hex');
        crypted += cipher.final('hex');
        message = crypted;
        if(this.checkMessageLength(message)){          
          this.setState({
            progress: 'Error',
            error:'Error :: Message length is a max total of 255 characters after encryption.'
          });
        }
        /*
        const decipher = crypto.createDecipher(cipherAlgo, passphrase);
        let decrypted = decipher.update(message, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        console.log('decrypted', decrypted);
        */
    }
    if(this.state.progress === undefined){
      this.setState({progress:'Sending'});
      this.props.sendTransaction(receiver, auth.wallet, Number(amount).toFixed(6), stakeAmount, message, history);   
    }
  }
  
  resetForm() {
    this.setState({ progress:undefined, error:undefined, showCamera:false, encrypted:false });
  }
  
  renderEncryptedState(){
     return this.state.encrypted ? <small>Encrypted  : </small> : <small>Plain Text  : </small>;
  }
  
  renderAlert() {
    if (!this.props.error) return null;
    return (
        <div className="alert alert-danger" role="alert">
            <h3 className="color14">{this.props.error}</h3>
        </div>   
    );
  }
  
  handleScan(data){
    if(data){
      this.setState({
        result: data,
        delay:0,
        showCamera:!this.state.showCamera
      })
    }
  }
  
  handleError(error){
    console.log(error);
    this.setState({error});
  }  
  
  handleCameraToggle(){
    this.setState({showCamera:!this.state.showCamera});
  }
  
  renderQRReader() {
    if(this.state.result !== 'No result'){
      this.props.changeFieldValue(this.props.form, 'receiver', this.state.result.split('\n').join(' '))
    }
    if(this.state.showCamera) {
      return(
        <div>
          <QrReader
            delay={this.state.delay}
            onError={this.handleError}
            onScan={this.handleScan}
            style={{ width: '100%', maxWidth: '300px' }}
            />
          <p>{this.state.result}</p>
        </div>
      );
    } else {
      return '';
    }
  }
  
  renderCipherSelect() {
    if(this.state.encrypted)
    return(<div className="row pb-3">           
            <div className="input-group col-6 pr-auto">
                <Field component="select" name="cipherAlgo" className="form-control">{this.renderCipherOptions()}</Field>
            </div>
            <div className="input-group col-6 pl-auto">
                <Field component="input" type="text" name="passphrase" placeholder="Enter a Passphrase" className="form-control" aria-label="passphrase" aria-describedby="basic-addon1"/>                
                <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon1" title="Generate Random Salt" onClick={this.handleGeneratePassphrase.bind(this)}><i className="fa fa-refresh" ref="saltSpinner" /></span>
                </div>
            </div>
            </div>);
            
  }
  
  renderCipherOptions(){
    return crypto.getCiphers().map((cipher,i) => {
	if(i === 0)
	        return <option key={i} defaultValue={cipher} >{cipher}</option>;

	return <option key={i} value={cipher}>{cipher}</option>;
    });
  }
  
  handleEncryptionToggle(){
    this.setState({encrypted:!this.state.encrypted});
  }
  
  render() {
    return (
      <div>
      <Header/>
    	<div className="container-fluid" id="aboutus">
        <div className="container">

      <section className="grey_bg">
 				<div className="row">
 					<div className="col">
 						<h3 className="green">SEND KEYSTONE
                <span className="float-right">
                  {this.renderEncryptedState()} &nbsp; <span className="switch switch-sm">
                    <input type="checkbox" className="switch" id="switch-sm" onChange={this.handleEncryptionToggle.bind(this)}/>
                    <label htmlFor="switch-sm"> </label>
                  </span>
                </span></h3>	
 					</div>	
 				</div>	
 				<form onSubmit={this.handleSubmit(this.handleFormSubmit.bind(this))}>
 				<div className="row">
 					<div className="col">
 							<div className="form-check form-group">
							  <label className="form-check-label color16 w-100" htmlFor="exampleRadios1">
								Make sure this is correct or you will lose your Keystone.
							  </label>
                {this.renderAlert()}
              </div>
              {this.renderCipherSelect()}
						  <div className="form-group">
                <div className="input-group ">                
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1" title="Look Up From Address Book" onClick={()=>{alert('Address Book View Popup')}}><i className="fa fa-address-book"/></span>
                    </div>
                    <Field name="receiver" component="input" type="text"  className="form-control" placeholder="Add receiver public key"/>
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon2" title="Scan QR" onClick={this.handleCameraToggle.bind(this)}><i className="fa fa-camera"/>{this.renderQRReader()}</span>
                    </div>
                </div>
						  </div>
						  <div className="form-row justify-content-center">
                <div className="col">
                  <Field name="amount" component="input" type="text" className="form-control" placeholder="US$"/>
                </div>
                <div className="badge badge-secondary align-self-center color8-bg">=</div>
                <div className="col">
                  <Field name="amount" component="input" type="text" className="form-control" placeholder="Keystone"/>
                </div>
						  </div>
						  <div className="m-3"></div>
						  <div className="form-group">
                <Field name="message" component="textarea" type="text" className="form-control" rows="3" placeholder="Message" />
						  </div>
 					</div>
 				</div>
 				<div className="row">
					<div className="col-12 col-sm-6 col-md-3 col-lg-3">            
            {this.state.progress && !this.props.error ? <p className="color6">Processing...</p> : <button className="btn btn-primary btn-large" type="submit" >Send</button>}
            {this.props.hash ? <h3 className="color6">{this.props.hash}</h3> : ''}
  				</div>
 				</div>
			</form>	
 			</section>
      </div>
      </div>
      <Footer />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ changeFieldValue, getLastTransaction, signTransaction, hashTransaction, sendTransaction}, dispatch);
}

const mapStateToProps = state => {
  return {
    users: state.users,
    progress:state.progress,
    auth: state.auth,
    error: state.transaction.error,
    hash: state.transaction.txHash,
    transaction: state.transaction
  };
};

SendTransaction = withRouter(connect(mapStateToProps, mapDispatchToProps)(SendTransaction));

export default reduxForm({
  form: 'send-transaction',
  fields: ['receiver', 'amount', 'stakeAmount', 'message', 'salt','passphrase','cipherAlgo']
})(SendTransaction);
