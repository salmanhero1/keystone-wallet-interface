const LATORTUGA_FAQ = [
    {
        q:"What is La Tortuga?",
        a:"La Tortuga will be the first eco-friendly, off-grid 21st century city. A full service community with hospitals attracting the world's best practitioners, schools staffed by the best educators, a place where utilities such as power and water, waste management, heating and air conditioning have been designed from the beginning to achieve the minimum carbon footprint whilst maximising the standard of living for all our residents."
    },
    {
        q:"Where is La Tortuga located?",
        a:"La Tortuga is located in Puerto Peñasco municipality in the state of Sonora, Mexico."
    },
    {
        q:"How much has been invested in the La Tortuga development thus far?",
        a:"Around $20,000,000.00 USD"
    },
    {
        q:"How much land will be developed?",
        a:"3,000 hectares total. Phase 1 will consist of 500 hectares"
    },
    {
        q:"What stage of development is the land in?",
        a:"All zoning has been approved on the entire property for development."
    },
    {
        q:"Will there be a Cruise port near La Tortuga?",
        a:"The cruise port is being built as we speak. The Federal Government has recently budgeted and approved $80,000,000.00 to continue construction. Plus a recent coalition between the Puerto Penasco Government and the the Arizona Government in the United States was formed with sole tasc of getting the pier finished."
    },
    {
        q:"Will the US Dollar or Mexican Peso be accepted at La Tortuga?",
        a:"Keystone Currency will be the only accepted form of payment accepted for commerce and investment in the development."
    },
    {
        q:"How will vendors keep up with fluctuating price?",
        a:"Prices will fluctuate and we understand that. Vendors will be encouraged to lock in a price every 12 hours. Our goal is to implement technology that will allow for streamlined shopping. Those who work at La Tortuga and are paid in KEYS will not have to wait for 2 weeks to receive a paycheck; this will help alleviate the burden of fluctuating price."
    }
];



const KEYS_BASICS = [
    {
        q:"What is Keystone?",
        a:"Keystone is a currency that will be the primary form of payment in the La Tortuga community and other Keystone-related projects."
    },
    {   
        q:"Can I mine Keystone?",
        a:"Yes, mining Keystone, or KEYS, will be open to the public once the blockchain code is released. Mining software can be developed, or the mining program developed by the Keystone blockchain developers can be used."
    },
    {
        q:"What is the maximum coin supply?",
        a:"Keystone's maximum supply is 150,000,000."
    },
    {
        q:"Can I setup a node?",
        a:"Yes. See our tutorial here. <a href=\"https://www.dropbox.com/s/y69alf9wenr3431/Setup Node.txt?dl=0\">Follow this wiki</a>"
    },
    {
        q:"How do I access my Keystone?",
        a:"You can access the Keystone blockchain using any web interface application that has been developed using Keystones open source software. Specification for development can be found here."
    },
    {
        q:"Where can I keep my Keystones?",
        a:"You don't need to \"keep your keystones\" anywhere. They are always on the chain. As long as you keep your wallet (PEM file) safe, your coins are safe.  Remember, if you lose your wallet file, your coins will NOT be retrievable."
    },
    {
        q:"Will there be a downloadable wallet?",
        a:"Not yet.  We are building a brand new blockchain from scratch..  We are not forking or copying another coin with core wallet features.  We are starting with a web-based wallet using RSA signatures.  This will allow us to be compatible on almost every device.  We will look to release downloadable wallets in the future. But with it being written in javascript, you will be able to run a node or miner on almost any device."
    },
    {
        q:"Who owns Keystone?",
        a:"No one owns the chain code, it will be open source, but Keystone Currency Inc. owns the exchange."
    },
    {
        q:"Is the Keystone source public?",
        a:"We are keeping the source code private until launch. It will be published on Keystone's full release."
    },
    {
        q:"Where can I find the whitepaper?",
        a:"Our full whitepaper will be released soon. For now, see our litepaper linked above for a broad view of Keystone Currency."
    },
    {
        q:"Will there be a bounty program?",
        a:"Yes. We have plans for a bounty program. Stay tuned."
    }];

const KEYS_SECURITY = [
    {
        q:"How do I secure my Keystone?",
        a:"Your Keystones are secured on the chain, by your private key. Keep you private key safe and your coins remain safe."
    },
    {
        q:"I have lost my private key, is there any way to retrieve it?",
        a:"There is no way to retrieve your private key, which means there is no way to retrieve your coins."
    },
    {
        q:"How do I send Keystone?",
        a:"You can send a transaction by signing into your wallet using your private key (PEM file) and navigating to the 'Send' tab that appears in the navigation bar."
    }];

const KEYS_MINING = [
    {
        q:"How does Keystone's mining work?",
        a:"Keystone will first be mined through the usual Proof-of-Work (POW) method. We will move to Proof of Stake (PoS) once we reach 35 million coins in circulation."
    },
    {
        q:"What is Proof of Stake?",
        a:"Proof of Stake (PoS) is a type of algorithm by which a cryptocurrency blockchain network aims to achieve distributed consensus. In PoS-based cryptocurrencies, the creator of the next block is chosen via various combinations of random selection and wealth or age (i.e. the stake)."
    },
    {
        q:"When does Proof of Stake start?",
        a:"Proof of Stake will start when circulation supply hits 35 million, or you see a vampire."
    },
    {
        q:"What are the Proof of Stake rewards?",
        a:"Our PoS system has not been worked out yet, therefore we cannot reveal the rewards just yet."
    },
    {
        q:"What sort of rig will I need to mine for the Bcrypt algo?",
        a:"We are using the bcrypt algorithm to enable fair mining for all. Bcrypt should make it good for all to mine, whether you have a powerful rig or not."
    },
    {
        q:"How will moving from POW to PoS be handled?",
        a:"At 35 million coins in circulation, the code will switch over seamlessly to our PoS system.  The code for the switch has been written, but as for the actual PoS rewards and finer details, we are yet to confirm those.  We are building an economy around this coin, so we have to make sure we get it right.  We have until the coin is mined to 35 million before we implement the PoS but we assure you this will be done long before we reach that."
    },    
    {
        q:"What are the fees when sending and receiving coins?",
        a:"Transaction fees vary from node to node, and from POW to PoS. See here for more information."
    }];

const KEYS_EXCHANGE = [
    {
        q:"What is the Keystone Exchange?",
        a:"The Keystone Exchange is your source for keystone while others create their 	exchanges."
    },
    {
        q:"Which trading pairs does the Keystone Exchange support?",
        a:"At the beginning the Keystone Exchange will support KEYS/FIAT. Later on there will be a KEYS/BTC pair along with other cryptocurrencies."
    },
    {
        q:"When is Keystone Currency getting listed on an exchange?",
        a:"After the coin is launched, we will start the process of getting listed on exchanges, whilst building our own exchange.  <br/> To get listed, we will need to prove our coins worth by way of a good product and a great community so this is something we are working on continuously."
    },];

const FURTHER_INFO = [
    {
        q:"What are the official sources of news from Keystone?",
        a:"<a href=\"https://twitter.com/Keystone_KEYS\">Twitter</a> | <a href=\"https://www.facebook.com/KeystoneCurrency/\">Facebook</a> | <a href=\"https://www.instagram.com/keystonecurrency/\">Instagram</a> | <a href=\"https://discord.gg/vjbXeHf\">Discord</a> | <a href=\"https://t.me/joinchat/AAAAAEyeJE3cAzcZjG8OHg\">Telegram</a> | <a href=\"https://www.reddit.com/user/KeystoneCurrency/\">Reddit</a>"
    },
    {
        q:"How many people are working on the Keystone Currency project?",
        a:"We have a team of 13 actively working on this project ranging from marketers and writers to experienced web and programming devs. We cover all bases but we're always looking for top talent to contribute."
    }];


module.exports = { KEYS_BASICS, KEYS_SECURITY, KEYS_MINING, KEYS_EXCHANGE, FURTHER_INFO, LATORTUGA_FAQ };