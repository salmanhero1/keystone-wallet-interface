import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import fileDownload from 'js-file-download';
import {
  signOut,
  getAlerts,
  toggleAlert,
  getAddressBook,
  } from '../actions';
import {
  Container,
  Row,
  Col,
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  UncontrolledDropdown,
  Alert
  } from 'reactstrap';
import logo from '../assets/images/logo.png';
import facebook from '../assets/images/facebook.png';
import twitter from '../assets/images/twitter.png';
import instagram from '../assets/images/instagram.png';
import discord from '../assets/images/discord.png';
import forumimage from '../assets/images/forum.png';
import telegram from '../assets/images/telegram.png';
import reddit from '../assets/images/reddit.png';
import youtube from '../assets/images/youtube.png';
const stylePointer = { cursor:"pointer"};
  
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
    this.toggle = this.toggle.bind(this);
    this.getAlerts = this.props.getAlerts.bind(this);
    this.getAddressBook = this.props.getAddressBook.bind(this);
    /*
    this.props.dangerAlert({
      alert_type: 'danger',
      visible: true,
      _html: <span><strong>Warning : </strong> We you browser storage and cookies, by using this site you agree to allowing us to use this data.</span>
      }
      ,this.props.alerts);
    */
  }
  
  componentWillMount(){
    this.getAlerts();
    if(this.props.authenticated)
        this.getAddressBook();
  }
  
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  
  signOut(){    
    this.props.signOut();
    this.props.history.push('/');
  }
  
  saveToLocalStorage () {
    localStorage.setItem('privateKey', this.props.auth.wallet.walletPrivateKey);
  }
   
  getLinks() {
    if (this.props.authenticated) {
      return [
        <NavItem key={1}>
          <NavLink tag={Link} to="/Block" style={{textShadow: '2px 2px 2px black'}}>Explorer</NavLink>
        </NavItem>,
        <NavItem key={2}>
          <NavLink tag={Link} to="/Wallet" style={{textShadow: '2px 2px 2px black'}}>Wallet&nbsp;Overview</NavLink>
        </NavItem>,
        <NavItem key={3}>
          <NavLink tag={Link} to="/Wallet/Send-Transaction" style={{textShadow: '2px 2px 2px black'}}>Send</NavLink>
        </NavItem>,
        <UncontrolledDropdown key={4} className="nav-item" style={{cursor:"pointer"}}>
            <DropdownToggle tag="nav-item">
                <NavLink style={{textShadow: '2px 2px 2px black'}}>Address Book </NavLink>
            </DropdownToggle>
            <DropdownMenu>
                <DropdownItem data-toggle="modal" data-target="#AddressBookModal" style={{cursor:"pointer"}}>View</DropdownItem>
                <DropdownItem onClick={this.downloadAddressBook.bind(this)} style={{cursor:"pointer"}}>Download</DropdownItem>          
            </DropdownMenu>
        </UncontrolledDropdown>,
        <NavItem key={5}>
            <NavLink tag={Link} to="/Roadmap" style={{textShadow: '2px 2px 2px black'}}>Roadmap</NavLink>
        </NavItem>,
        <NavItem key={6}>
          <NavLink tag={Link} to="/FAQ" style={{textShadow: '2px 2px 2px black'}}>FAQ</NavLink>
        </NavItem>,
   <NavItem key={7}>
          <NavLink tag={Link} to="/About-Us" style={{textShadow: '2px 2px 2px black'}}>About&nbsp;Us</NavLink>
        </NavItem>,
   <NavItem key={8}>
          <NavLink tag={Link} to="/Litepaper" style={{textShadow: '2px 2px 2px black'}}>Litepaper</NavLink>
        </NavItem>
      ];
    }
    return [
      
	<NavItem key={1}>
        <NavLink tag={Link} to="/" className="d-block d-md-none d-lg-none d-xl-none" style={{textShadow: '2px 2px 2px black'}}>Home</NavLink>
      </NavItem>,
	<NavItem key={2}>
        <NavLink tag={Link} to="/Block" style={{textShadow: '2px 2px 2px black'}}>Explorer</NavLink>
      </NavItem>,
      <NavItem key={3}>
          <NavLink tag={Link} to="/Roadmap" style={{textShadow: '2px 2px 2px black'}}>Roadmap</NavLink>
      </NavItem>,
      <NavItem key={4}>
          <NavLink tag={Link} to="/FAQ" style={{textShadow: '2px 2px 2px black'}}>FAQ</NavLink>
      </NavItem>,
   <NavItem key={5}>
          <NavLink tag={Link} to="/Litepaper" style={{textShadow: '2px 2px 2px black'}}>Litepaper</NavLink>
        </NavItem>,
      <NavItem key={6}>
          <NavLink tag={Link} to="/About-Us" style={{textShadow: '2px 2px 2px black'}}>About&nbsp;Us</NavLink>
      </NavItem>,
    ];
  }
  
  onDismiss(e) {
      let alertId = e.target.parentElement.dataset.alertid;
      if(alertId === undefined)
        alertId = e.target.parentElement.parentElement.dataset.alertid;
      if(alertId !== undefined) {
        const alerts = this.props.alerts.slice();
        alerts[alertId].visible = !alerts[alertId].visible;        
        this.props.toggleAlert(alerts);
      }
  }
    
  clearLocalStorage () {
      localStorage.setItem('privateKey', null);
  }
  
  downloadFile() {
    fileDownload(this.props.auth.wallet.walletPrivateKey, 'KeystoneCurrencyWallet.pem');
  }
  
  downloadAddressBook() {
    fileDownload(JSON.stringify(this.props.addressBook), 'AddressBook.json');    
  }
  
  render() {
    return (
      <div>
        <section>
        	<Container fluid className="bg-dark" id="top_menu">
        		<Container>
        			<Row>
                <Col id="social_icons">
                  <div><a href="https://www.facebook.com/KeystoneCurrency/"><img src={facebook} alt="Keystone Facebook"/></a></div>
                  <div><a href="https://twitter.com/Keystone_KEYS"><img src={twitter} alt="Keystone Twitter"/></a></div>
                  <div><a href="https://www.instagram.com/keystonecurrency/"><img src={instagram} alt="Keystone Instagram"/></a></div>
                  <div><a href="https://discord.gg/sJvgZ8K"><img src={discord} alt="Keystone Discord"/></a></div>
                  <div><a href="https://bitcointalk.org/index.php?topic=3238352.0"><img src={forumimage} alt="Keystone Bitcointalk"/></a></div>
                  <div><a href="https://t.me/joinchat/AAAAAEyeJE3cAzcZjG8OHg"><img src={telegram} alt="Keystone Telegram"/></a></div>
                  <div><a href="https://www.reddit.com/r/KeystoneCurrency/"><img src={reddit} alt="Keystone Reddit"/></a></div>
                  <div><a href="https://www.youtube.com/channel/UClLgDTUOlBtVccz8d7I1AJQ"><img src={youtube} alt="Keystone Youtube"/></a></div>
                </Col>
              {(this.props.authenticated) ?
                <Col className="text-right align-self-center">
        				  <Link to="#" style={stylePointer} onClick={this.signOut.bind(this)}>Sign-Out</Link> | <Link to="#" style={stylePointer} onClick={this.clearLocalStorage.bind(this)}>Clear Browser Key</Link> | <Link to="#" onClick={this.saveToLocalStorage.bind(this)} style={stylePointer}>Save Key To Browser</Link>  | <Link to="#" style={stylePointer} onClick={this.downloadFile.bind(this)}>Download Key</Link> 
        				</Col>                
                : 
        				<Col className="text-right align-self-center">
        					<Link to="/Sign-In" style={{stylePointer,textShadow: '2px 2px 4px limegreen'}}>Sign-In</Link> | <Link to="/Wallet/Create" style={{stylePointer, textShadow: '2px 2px 4px limegreen'}}>Create A Wallet</Link>
        				</Col>
                }
        			</Row>
        		</Container>
        	</Container>
        </section>
        <Container fluid className="d-none d-md-block" id="top_area">
       		<Container>
       			<Row >
       				<Col className="text-center align-self-center my-2 or my-2">
                  <Link to="/"><img src={logo} alt="Keystone Currency Blockchain"/></Link>              
              </Col>
       			</Row>
       		</Container>
       	</Container>
        <Container fluid className="mx-0 px-0" id="navigation">
          <Container >
            <Row>
              <Col>
                <div className="navbar--top" style={{zIndex:1070}}>
                   <Navbar color="faded" light expand="md">
                   <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                      <Nav className="mx-auto" navbar>
                        {this.getLinks()}
                      </Nav>
                    </Collapse>
                  </Navbar>
                </div>
              </Col>
            </Row>
          </Container>
        </Container>
        {this.props.alerts.length ?  
        <Container fluid className="mt-2 px-0">
          <Container >
            <Row>
              <Col>
              {this.props.alerts.map((alert,i) => {                
                return(<Alert color={alert.alert_type} isOpen={alert.visible} toggle={this.onDismiss.bind(this)} data-alertid={i} key={i}>
                  {alert._html}                
                </Alert>);   
              })}
              </Col>
            </Row>
          </Container>
        </Container> : ''}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    addressBook : state.addressBook,
    alerts : state.alerts,
    auth:state.auth,
    authenticated: state.auth.authenticated    
  };
};

export default withRouter(connect(mapStateToProps, {
  signOut,
  getAlerts,
  toggleAlert,
  getAddressBook,
  })(Header));
