import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Header from "./Header";
import Footer from "./Footer";
import {
  Nav,
  NavItem,
  NavLink,
  ListGroup,
  Card,
  CardBody,
  CardHeader,
  CardTitle
} from "reactstrap";
import * as FAQ_keys from "./FAQ_JSON";
class FAQ extends Component {
  render() {
    return (
      <div className="">
        <Header />
        <section>
          <div className="container-fluid" id="aboutus">
            <div className="container">
              <div className="row pb-0">
                <div className="col text-center">
                  <h1 className="green">FAQs</h1>
                  <p className="color7">
                    Frequently asked questions about the Keystone blockchain and
                    the La Tortuga community.
                  </p>
                </div>
              </div>
              <Nav tabs>
                <NavItem id="faq-tab">
                  <NavLink
                    active
                    className="color18"
                    href="#KEYS_BASICS"
                    data-toggle="tab"
                  >
                    <h4>
                      <strong>Basic</strong>
                    </h4>
                  </NavLink>
                </NavItem>
                <NavItem id="faq-tab">
                  <NavLink
                    className="color18"
                    href="#KEYS_SECURITY"
                    data-toggle="tab"
                  >
                    <h4>
                      <strong>Security</strong>
                    </h4>
                  </NavLink>
                </NavItem>
                <NavItem id="faq-tab">
                  <NavLink
                    className="color18"
                    href="#KEYS_MINING"
                    data-toggle="tab"
                  >
                    <h4>
                      <strong>Mining</strong>
                    </h4>
                  </NavLink>
                </NavItem>
                <NavItem id="faq-tab">
                  <NavLink
                    className="color18"
                    href="#KEYS_EXCHANGE"
                    data-toggle="tab"
                  >
                    <h4>
                      <strong>Exchange</strong>
                    </h4>
                  </NavLink>
                </NavItem>
                <NavItem id="faq-tab">
                  <NavLink
                    className="color18"
                    href="#LATORTUGA_FAQ"
                    data-toggle="tab"
                  >
                    <h4>
                      <strong>La Tortuga</strong>
                    </h4>
                  </NavLink>
                </NavItem>
                <NavItem id="faq-tab">
                  <NavLink
                    className="color18"
                    href="#FURTHER_INFO"
                    data-toggle="tab"
                  >
                    <h4>
                      <strong>Further Information</strong>
                    </h4>
                  </NavLink>
                </NavItem>
              </Nav>

              <div className="tab-content">
                {Object.keys(FAQ_keys).map((key, i) => {
                  return (
                    <ListGroup
                      className={`tab-pane fade show ${ i === 0 ? "active" : null }`}
                      id={`${key}`}
                      key={`${key}-${i}`}
                      aria-multiselectable="true"
                      role="tablist"
                    >
                      {FAQ_keys[key].map((question, j) => {
                        
                        return (
                          <Card key={j}>
                            <CardHeader role="tab" color="secondary" data-toggle="collapse"
                                    data-parent={`#${key}`}
                                    href={`#${key}_FAQ_${j}`}
                                    aria-expanded="false"
                                    aria-controls={`${key}_FAQ_${j}`}>
                                <CardTitle className="m-0 color18">{question.q}</CardTitle>
                            </CardHeader>
                            <div
                                id={`${key}_FAQ_${j}`}
                                className="collapse"
                                role="tabpanel"
                                aria-labelledby={`${key}${j}`}>
                                <CardBody>
                                    <p dangerouslySetInnerHTML={{__html: question.a}}/>
                                </CardBody>
                            </div>
                          </Card>
                        );
                      })}
                    </ListGroup>
                  );
                })}
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}
export default withRouter(FAQ);
