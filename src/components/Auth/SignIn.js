import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { login } from '../../actions';
import { connect } from 'react-redux';
import Header from '../Header';
import Footer from '../Footer';
class SignIn extends Component {
  
  handleUploadFile(event){
    const reader = new FileReader();
    reader.readAsBinaryString(event.target.files[0]);
    reader.onload = () => {        
        const privateKey = reader.result;
        this.props.login(privateKey, this.props.history);
    };
  }
  
  renderAlert() {
    if (!this.props.error) return null;
    return (
        <div className="alert alert-danger" role="alert">
            <h3 className="color14">{this.props.error}</h3>
        </div>   
        );
  }
  
  loadKeyFromLocalStorage() {
    this.props.login(localStorage.getItem('privateKey'), this.props.history);
  }
  
  render() {
    return (
    <div>
      <section>
          <Header />
      </section>
      <section>
        <div className="container-fluid" id="aboutus">
          <div className="container">
            <div className="row ">
              <div className="col text-center">
                <h1 className="green">Login To Your <span style={{textShadow: '2px 2px 4px black'}}>Keystone</span> Wallet</h1>
                <p className="color16">Note: Don&#39;t lose your wallet! You want to store your private key in a safe place for accessing your wallet.</p>
              </div>
            </div>
            {this.renderAlert()}
            <div className="row justify-content-center text-center">
              <div className="col align-self-center">
                <button className="btn btn-lg green_bg w-100" onClick={this.loadKeyFromLocalStorage.bind(this)}>Browser Login</button>
              </div>
              <div className="badge badge-secondary align-self-center color8-bg">OR</div>
              <div className="col ">
                <input className="btn btn-lg green_bg w-100" onChange={this.handleUploadFile.bind(this)} name="privateKeyFile" type="file"/>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </div>);
  }
}

const mapStateToProps = state => {
  return {
    error: state.auth.error,
    authenticated: state.auth.authenticated
  };
};

SignIn = connect(mapStateToProps, { login })(SignIn);

export default reduxForm({
  form: 'signin',
  fields: ['privateKey']
})(SignIn);
