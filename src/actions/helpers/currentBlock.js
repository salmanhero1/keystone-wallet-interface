import axios from "axios";

import HOST_URL from "../../configs";

import { GET_CURRENT_BLOCK , LAST_BLOCK_LOOKUP_ERROR } from '../../actions';

export const getCurrentBlock = () => {
  return dispatch => {
    axios
      .get(`${HOST_URL}/Block/Last-Block`)
      .then(response => {
        return dispatch({
          type: GET_CURRENT_BLOCK,
          payload: response.data.blockId
        });
      })
      .catch(() => {
        return dispatch({
          type: LAST_BLOCK_LOOKUP_ERROR,
          payload: 0
        });
      });
  };
};
