import { TOGGLE_ALERT, ALERT_DANGER, ALERT_DEFAULT, ALERT_ERROR, ALERT_INFO, ALERT_PRIMARY, ALERT_SUCCESS, ALERT_WARNING } from '../actions';

export default (alerts = [], action) => {
    switch (action.type) {
      case ALERT_DEFAULT:
      case ALERT_PRIMARY:
      case ALERT_INFO:
      case ALERT_SUCCESS:
      case ALERT_WARNING:
      case ALERT_ERROR:
      case ALERT_DANGER:
      case TOGGLE_ALERT:
        return action.payload;
      default:
        return alerts;
    }
};